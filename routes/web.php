<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test

Route::get('/', 'PostController@home');
Route::get('/naujas', 'PostController@forma');
Route::post('/post','PostController@saugok');
Route::get('/','PublicPostController@paiimk');
Route::get('/post/{post}','PublicPostController@showPost');

Route::get('/post/{post}/edit','PostController@editPost');
Route::patch('/post/{post}/','PostController@updatePost');
Route::get('/post/{post}/delete','PostController@deletePost');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard','PostController@admin');


Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/post/{post}/comment','CommentController@addComment');
