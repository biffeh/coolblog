<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function forma () {
        return view('pages.forma');
    }
    public function saugok()
    {
        $this->validate(request(),
            [
                'title'=> 'required',
                'body'=> 'required'
            ]);
        Post::create(['title'=>request('title'),
            'body'=>request('body'),
            'user_id'=>auth()->id()
        ]);
        return redirect('/dashboard');
    }
    public function home() {
        return view('pages.home');
    }

    public function editPost(Post $post)
    {
        if (Gate::denies('edit-post',$post))
        {
            return view('pages.restrict');
        }
        return view('pages.edit-post',compact('post'));
    }
    public function updatePost(Request $request, Post $post)
    {

        Post::where('id',$post->id)->update($request->only(['title','body']));
        return redirect('/dashboard');
    }
    public function deletePost(Post $post)
    {

        $post->delete();
        return redirect('/dashboard');
    }
    public function admin()
    {
        $posts = Post::all();
        return view('pages.dashboard',compact('posts'));
    }



}
