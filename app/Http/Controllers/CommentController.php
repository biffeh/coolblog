<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;

class CommentController extends Controller
{
    public function addComment(Post $post)
    {
//        $this->validate(request(),
//            [
//                'title'=> 'required',
//                'body'=> 'required'
//            ]);
        Comment::create([
            'body' => request('body'),
            'post_id'=>$post->id,
            'user_id'=>auth()->id()
        ]);
        return back();
    }
}
