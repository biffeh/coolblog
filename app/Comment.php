<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;


class Comment extends Model
{
     protected $fillable = ['user_id','post_id','body'];
//
//    protected $protected = ['updated_at', 'created_at'];

    public function comments()
    {
        return $this->belongsTo(Post::class);
    }
}
