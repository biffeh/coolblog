
@extends('layouts/main')
@section('content')

    <h2>Irasu sarasas</h2>
    <table class="table table-bordered">
    <tr>
    <th>Title</th>
    <th>Content</th>
        <th>Buttons</th>
    </tr>
        @foreach($posts as $post)
        <tr>
        <td>{{$post->title}}</td>
        <td><p>{{str_limit($post->body, 100)}}</p></td><br>
        <td><a class="btn btn-default" href="/post/{{$post->id}}" role="button">More..</a>
            <a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Edit</a>
            <a class="btn btn-danger" onclick="return confirm('Ar tikrai norite istrinti?')" href="/post/{{$post->id}}/delete" role="button">Delete</a></td>
        </tr>

    @endforeach
    </table>
    <a class="btn btn-success" href="/naujas" role="button">Create a new Post</a>
    {{--{{$posts->links()}}--}}
@endsection
