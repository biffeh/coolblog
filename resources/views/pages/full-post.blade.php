@extends ('layouts/main')
@section ('content')


    <h2>{{$post->title}}</h2>
    <p>{{$post->body}}</p>

    {{--<div class="comments">--}}
        {{--<h4>Komentarai</h4>--}}
        {{--<ul class="list-group">--}}
            {{--@foreach($post->comments as $comment)--}}
                {{----}}
                {{--<li class="list-group"><strong> {{$comment->created_at}}</strong> {{$comment->body}} </li>--}}
                {{--<hr>--}}
            {{--@endforeach            --}}
        {{--</ul>--}}
        {{----}}
    {{--</div>--}}
    <div id="comments" class="comments">
        <h4>Komentarai</h4>
        <ul class="list-group">
        @foreach($post->comments as $comment)
            <hr>
            <ul class="comment">

                {{--<h4 class="pull-left">John</h4>--}}
                <p class="pull-right"> {{$comment->created_at}}</p>

            <p>
                <em>{{$comment->body}}</em>
            </p>
        </ul>
        <hr>
            @endforeach
            </ul>
    </div>

    <hr>
    <div class="cards">
        <div class="card-block">
            <form action="/post/{{$post->id}}/comment" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="body" placeholder="Prasom nekomentint" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Nekomentuok sito</button>
                </div>
            </form>
        </div>
    </div>
        @if(Auth::id() == $post->user_id)
        <p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Edit</a></p>
        <p><a class="btn btn-danger" href="/post/{{$post->id}}/delete" role="button">Delete</a></p>
        @endif
@endsection

