@extends ('layouts/main')
@section ('content')
    @include('inc/error')
    <div class="content">
        <div class="title m-b-md">
            <h2>Naujas straipsnis</h2>
        </div>
        <form action="/post" method="post" class="form-horizontal" >
            {{csrf_field()}}
            <div class="form-group">
                <label class="col-sm-2 control-label" for="title">Title</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="title" id="title">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="body">Content</label>
                <div class="col-sm-10">
        <textarea class="form-control" type="text" name="body" id="body">
        </textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit">Įrašyti</button>
                </div>
            </div>
        </form>
        <div class="col-sm-10 col-sm-offset-2">
            <div class="links">
                <a href="/">Grįžti namo</a>
            </div>
        </div>
    </div>
@endsection