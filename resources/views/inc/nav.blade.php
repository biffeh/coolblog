<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Blogas</a>
            @guest
                <a class="navbar-brand" href="/login">Login</a>
                <a class="navbar-brand" href="/register">Register</a>
            @endguest

        </div>
        <div id="navbar" class="navbar-collapse collapse">
            @if (Auth::check())

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello {{Auth::user()->name}} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/dashboard">Dashboard</a></li>
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="/naujas">New entry</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout">Logout</a></li>

                        </ul>
                    </li>
                </ul>

            @else
                <form class="navbar-form navbar-right" method="post" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                    </div>

                    <div class="form-group">

                        <input type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </form>
            @endif
        </div><!--/.navbar-collapse -->
        @if ($errors->has('email'))
            <div class="invalid-feedback alert alert-warning">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        @if ($errors->has('password'))
            <div class="invalid-feedback alert alert-warning">
                <strong>{{ $errors->first('password') }}</strong>
            </div>
        @endif

    </div>
</nav>